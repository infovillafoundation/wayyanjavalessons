package org.bitbucket.infovillafoundation.wayyan.personalitytest;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.datafx.controller.flow.Flow;
import org.datafx.controller.flow.action.FlowActionChain;
import org.datafx.controller.flow.action.FlowLink;
import org.datafx.controller.flow.action.FlowMethodAction;

/**
     * Created by Admin on 10/12/2014.
     */
    public class Main extends Application {
        public static void main(String[] args) {
            launch(args);
        }

        @Override
        public void start(Stage primaryStage) throws Exception {
            primaryStage.getIcons().add(new Image("/images/icon.png"));
            new Flow(WelcomeController.class)
                    .withAction(QuestionController.class, "chooseAndNext", new FlowActionChain(new FlowMethodAction("makeChoice"), new FlowLink<>(FinishController.class)))
                    .startInStage(primaryStage);
        }
}
