package org.bitbucket.infovillafoundation.wayyan.personalitytest;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.LinkAction;

/**
 * Created by Admin on 14/12/2014.
 */

@FXMLController(value = "/fxml/welcome.fxml", title="Personality Test")
public class WelcomeController {
    @FXML
    private Text welcome;

    @FXML
    private Text welcomerequest;

    @FXML
    @LinkAction(QuestionController.class)
    private Button start;
}