package org.bitbucket.infovillafoundation.wayyan.personalitytest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 14/12/2014.
 */

public class Questions {
    int totalNo = 50;
    int questionNumber = 0;
    List<String> questions = new ArrayList<>();
    List<String> choices = new ArrayList<>();

    public void addChoice(int questionNumber, String[] choice) {
        choices.add(questionNumber - 1, choice.toString());
    }

    public void addQuestion(int questionNumber, String question) {
        questions.add(questionNumber - 1, question);
    }

    public int getTotalNo() {
        return totalNo;
    }
}
