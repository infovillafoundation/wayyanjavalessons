package org.bitbucket.infovillafoundation.wayyan.personalitytest;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Toggle;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;
import org.datafx.controller.flow.action.BackAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by Admin on 14/12/2014.
 */

@FXMLController(value="/fxml/question.fxml", title="Questions")
public class QuestionController {
    @Inject
    private Questions questions;

    @FXML
    private Text question;

    @FXML
    @BackAction
    private Button back;

    @FXML
    @BackAction
    private Button back2;

    @FXML
    @ActionTrigger("chooseAndNext")
    private Button next;

    @FXML
    @ActionTrigger("chooseAndNext")
    private Button finish;

    @PostConstruct
    public void init() {
        if (questions.questionNumber > 1 && questions.questionNumber < 50) {
            back.setVisible(false);
            back2.setVisible(true);
        }
        else if (questions.questionNumber == 50) {
            next.setVisible(false);
            next.setVisible(true);
        }
    }
}