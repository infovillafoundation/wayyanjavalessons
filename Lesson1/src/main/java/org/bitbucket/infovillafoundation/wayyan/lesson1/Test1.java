package org.bitbucket.infovillafoundation.wayyan.lesson1;

/**
 * Created by Admin on 2/12/2014.
 */

import java.util.ArrayList;
import java.util.List;

public class Test1 {
    public static String main(String[] args) {
        List<Table> tables = new ArrayList<Table>();

        for (int i = 0; i < 10; i++) {
            Table table = new Table(i + 1, "round", 5, "normal", true);
            tables.add(table);
        }

        for (Table table : tables) {
            System.out.println(table);
        }

        List<Customer> customers = new ArrayList<Customer>();

        Customer [] customer = new Customer[11];
        customer[1] = new Customer("John", "928372", 5);
        customer[2] = new Customer("Alex", "203948", 5);
        customer[3] = new Customer("Ben", "483675", 5);
        customer[4] = new Customer("Harry", "694106", 5);
        customer[5] = new Customer("Daniel", "720165", 5);
        customer[6] = new Customer("Michael", "096815", 5);
        customer[7] = new Customer("Jackson", "162957", 5);
        customer[8] = new Customer("Collin", "710548", 5);
        customer[9] = new Customer("Joseph", "391056", 5);
        customer[10] = new Customer("Evan", "105809", 5);

        for (int i = 1; i < 11; i++)
            customers.add(customer[i]);

        for (Customer customer1 : customers) {
            System.out.println(customer1);
        }

        for (int i = 0; i < 10; i++) {
            tables.get(i).setCustomer(customers.get(i));
        }

        for (Table table : tables) {
            System.out.println(table);
        }


        System.out.println("sleepIn(false, false) -> " + Exercise1.sleepIn(false, false));
        System.out.println("diff21(21) -> " + Exercise1.diff21(21));
        System.out.println("nearHundred(150) -> " + Exercise1.nearHundred(150));
        System.out.println("missingChar(\"whatever\", 5) -> " + Exercise1.missingChar("whatever", 5));
        System.out.println("backAround(\"javascript\") -> " + Exercise1.backAround("javascript"));*/
    }
/*
public static void main(String[]args){
        System.out.println("max(5, 10) -> " + max(5, 10));
        System.out.println("\nmaxOfThree(5, 10, 15) -> " + maxOfThree(5, 10, 15));
        System.out.println("\nifVowel('a') -> " + ifVowel('a'));
        System.out.println("\nreplaceAndTranslate(\"i love javascript\") -> " + replaceAndTranslate("i love javascript"));
        System.out.println("\nsum([10, 20, 30]) -> " + sum([10, 20, 30]));
        System.out.println("\nmultiply([210, 20, 30]) -> " + multiply([10, 20, 30]));
        System.out.println("\nreverse(\"some random text") -> \" + reverse("some random text"));
        System.out.println("\nfindLongestWord(\"find the longest word here\") -> " + findLongestWord("find the longest word here"));
        System.out.println("\nfilterLongWords(\"extremely boring long words\", 5) -> " + filterLongWords("extremely boring long words", 5));
        System.out.println("\ncharFreq(\"abcdefghiijjkkll\") -> "+ charFreq("abcdefghiijjkkll"));
        System.out.println("\ntranslateChristmasCard(\"merry christmas and happy new year\") -> " + translateChristmasCard("merry christmas and happy new year"));
        }


    public static int max(int a, int b) {
        if (a > b)
            return a;
        else
            return b;
    }

    public int maxOfThree(int a, int b, int c) {
        return max(max(a, b), c);
    }

    public boolean ifVowel(char letter) {
        char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        for (int vowel : vowels)
            if (vowels[vowel] == letter)
                return true;
        return false;
    }


    public String translate(String text) {
        for (char letter : text)
            if (ifVowel(letter) == false)
                letter = letter + 'o' + letter;
        return text;
    }

    public int sum(int... list) {
        int sum = 0;
        for (int item : list)
            sum += list[item];
        return sum;
    }

    public int multiply(int...list) {
        int product = 1;
        for (int item : list)
            product *= list[item];
        return product;
    }

    public String reverse(String text) {
        String rev = "";
        for (int letter : text.toCharArray())
            rev = letter + rev;
        return rev;
    }

    public String translateChristmasCard(String text) {
        Map<String, String> lexicon = {"merry": "god", "christmas": "jul", "and": "och", "happy": "gott", "new": "nytt", "year": "år"};
        String currentWord = "";
        String result = "";
        for (char letter : text.toCharArray()) {
            if (letter == ' ') {
                for (String key : lexicon.keySet()) {
                    if (key == currentWord) {
                        currentWord = key;
                        result += currentWord + " ";
                        currentWord = "";
                    }
                }
            }
            else {
                currentWord += letter;
                System.out.println("currentWord : " + currentWord);
            }
        }
        result += currentWord;
        return result;
    }


    public int findLongestWord(String text) {
        int maxLength = 0;
        int wordLength = 0;
        for (char letter : text.toCharArray()) {
            if (letter == ' ') {
                if (wordLength > maxLength) {
                    maxLength = wordLength;

                }
                wordLength = 0;
            }
            else
                wordLength++;
        }
        return maxLength;
    }

    public String replaceAndTranslate(String word) {
        String result = "";
        for (char character : word.toCharArray()) {
            if (character != 'a' && character != 'e' && character != 'i' && character != 'o' && character != 'u' && character != ' ') {
                result += character + 'o' + character;
            }
            else {
                result += character;
            }
        }
        return result;
    };

    public String[] filterLongWords(String text, int i) {
        String currentWord = "";
        int longWords = String[];
        int index = 0;
        for (char character : text) {
            if (character == ' ') {
                if (currentWord.length() > i) {
                    index = currentWord;
                    index++;
                }
                currentWord = "";

            }
            else {
                currentWord += character;
            }
        }
        return longWords;
    }

    public static Map<String, Integer> charFreq(String text) {
        int frequency = {'a': 0, 'b': 0, 'c': 0, 'd': 0, 'e': 0, 'f': 0, 'g': 0, 'h': 0, 'i': 0, 'j': 0, 'k': 0, 'l': 0, 'm': 0, 'n': 0,
                'o': 0, 'p': 0, 'q': 0, 'r': 0, 's': 0, 't': 0, 'u': 0, 'v': 0, 'w': 0, 'x': 0, 'y': 0, 'z': 0};

        for (char letter : text) {
            for (String key : frequency)
                if (key.equals(letter))
                    key++;
        }
        for (String key : frequency)
            System.out.println(key + ": " + key);
    }

}*/

}
