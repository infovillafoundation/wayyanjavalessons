package org.bitbucket.infovillafoundation.wayyan.lesson1;

/**
 * Created by Admin on 2/12/2014.
 */

public class Exercise1 {
    public static boolean sleepIn(boolean weekday, boolean vacation) {
        return !weekday || vacation;
    }

    public static int diff21(int n) {
        int num = 21;
        if (n <= num)
            return num - n;
        else
            return (n - num) * 2;
    }

    public static boolean nearHundred(int n) {
        return (n >= 0 && n <= 10 + 10) || (n >= 100 - 10 && n <= 100 + 10) || (n >= 200 - 10 && n <= 200 + 10);
    }

    public static String missingChar(String str, int n) {
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            if (i == n) {
                continue;
            }
            result += str.toCharArray()[i];
        }
        return result;
    }

    public static String backAround(String str) {
        char lastLetter = str.toCharArray()[str.length() - 1];
        String newString;
        newString = lastLetter + str + lastLetter;
        return newString;
    }
}
