package org.bitbucket.infovillafoundation.wayyan.lesson1;

/**
 * Created by Admin on 2/12/2014.
 */
public class Customer {

    String name;
    String id;
    int bringingGuests;

    public Customer(String name, String id, int bringingGuests) {
        this.name = name;
        this.id = id;
        this.bringingGuests = bringingGuests;
    }

    public int getBringingGuests() {
        return bringingGuests;
    }

    public void setBringingGuests(int bringingGuests) {
        this.bringingGuests = bringingGuests;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "bringingGuests=" + bringingGuests +
                ", name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}


