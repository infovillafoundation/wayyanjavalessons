package org.bitbucket.infovillafoundation.wayyan.orm;

/**
 * Created by Admin on 8/12/2014.
 */
public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.shout();
    }

    public void shout() {
        Sub sub = new Sub();
        sub.superMethod();
        sub.cool();

        Super super1 = new Super();
        super1.superMethod();
        super1.cool();
    }

    public class Super {
        public void superMethod() {
            System.out.println("super");
        }

        public void cool() {
            System.out.println("super cool");
        }
    }

    public class Sub extends Super {

        public void subMethod() {
            System.out.println("sub");
        }

        @Override
        public void cool() {
            System.out.println("even more cool");
        }
    }
}
