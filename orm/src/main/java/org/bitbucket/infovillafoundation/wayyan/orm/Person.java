package org.bitbucket.infovillafoundation.wayyan.orm;

import java.math.BigInteger;

/**
 * Created by Admin on 8/12/2014.
 */

@Entity
public class Person {
    private Integer id;
    private String name;
    private Long age;
    private float height;
    private String comments;

    public Person(Integer id, String name, Long age, float height, String comments) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.height = height;
        this.comments = comments;
    }

    public Person() {
        id = 0;
        name = "";
        age = 0L;
        height = 0;
        comments = "";
    }
}
