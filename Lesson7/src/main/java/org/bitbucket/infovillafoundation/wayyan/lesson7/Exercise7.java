package org.bitbucket.infovillafoundation.wayyan.lesson7;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 13/12/2014.
 */

public class Exercise7 {
    public static String stringTimes(String str, int n) {
        String result = "";
        for (int i = 0; i < n; i++)
            result += str;
        return result;
    }

    public static boolean doubleX(String str) {
        boolean result = false;
        for (char letter : str.toCharArray())
            if (letter == 'x' && str.toCharArray()[str.indexOf(letter) + 1] == 'x')
                result = true;
        return result;
    }

    public static int last2(String str) {
        String last3 = str.substring(str.length() - 3);
        String secondString = str.substring(0, str.length() - 3);
        int repetitions = 0;
        for (char letter : secondString.toCharArray()) {
            if (str.indexOf(letter) < str.length() - 4)
                if (letter == last3.toCharArray()[1] && secondString.toCharArray()[str.indexOf(letter) + 1] == last3.toCharArray()[2])
                    repetitions += 1;
            else if (str.indexOf(letter) == str.length() - 3)
                if (str.toCharArray()[str.indexOf(letter) + 1] == last3.toCharArray()[0])
                    repetitions += 1;
        }
        return repetitions;
    }

    public static boolean array123(int[] nums) {
        boolean result = false;
        int consecutive = 0;

        for (int num : nums) {
            if (num == 1 && consecutive == 0) {
                consecutive = 1;
                continue;
            }
            else if (num == 2 && consecutive == 1) {
                consecutive = 2;
                continue;
            }
            else if (num == 3 && consecutive == 2) {
                result = true;
                break;
            }
            else
                consecutive = 0;
        }

        return result;
    }

    public static String altPairs(String str) {
        String result = "";
        List<Integer> nums = new ArrayList<Integer>();
        int a = 0;
        int b = 1;
        for (int i = 0; i < 10; i++) {
            nums.add(a);
            nums.add(b);
            a += 4;
            b += 4;
        }
        System.out.println(nums);

        for (int num : nums) {
            int index = 0;
            System.out.println(result += nums.toString().toCharArray()[index]);
            index++;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Exercise7.stringTimes("test", 4));
        System.out.println(Exercise7.stringTimes("hi", 1));
        System.out.println(Exercise7.doubleX("xoxo"));
        System.out.println(Exercise7.doubleX("xxxyyyzzz"));
        System.out.println(Exercise7.last2("hihihihi"));
        System.out.println(Exercise7.last2("aaaaa"));
        System.out.println(Exercise7.array123(new int[]{1, 2, 3, 1, 4}));
        System.out.println(Exercise7.array123(new int[]{1, 4, 3, 2, 3, 1}));
        System.out.println(Exercise7.altPairs("this is some test"));
        System.out.println(Exercise7.altPairs("hi"));
    }
}
