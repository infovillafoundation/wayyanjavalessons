package org.bitbucket.infovillafoundation.wayyan.lesson7;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 13/12/2014.
 */

public class Exercise7Comment {
    public static String stringTimes(String str, int n) {
        String result = "";
        for (int i = 0; i < n; i++)
            result += str;
        return result;
    }

    public static boolean doubleX(String str) {
        boolean result = false;
        for (char letter : str.toCharArray()) {
            if (letter == 'x' && str.indexOf(letter) + 1 < str.length() && str.toCharArray()[str.indexOf(letter) + 1] == 'x')
                    result = true;
        }
        return result;
    }

    public static int last2(String str) {
        String last2 = str.substring(str.length() - 2);
        String secondString = str.substring(0, str.length() - 2);
        int consecutive = 0;
        int repetitions = 1;
            for (char letter : secondString.toCharArray()) {
                if (letter == last2.toCharArray()[0])
                    consecutive = 1;
                if (consecutive == 1 && letter == last2.toCharArray()[1])
                    repetitions++;
                consecutive = 0;
        }
        return repetitions;
    }

    public static boolean array123(int[] nums) {
        int consecutive = 0;
        boolean result = false;
        for (int num : nums)
            switch (num) {
                case 1:
                    consecutive = 1;
                    break;
                case 2:
                    if (consecutive == 1)
                        consecutive = 2;
                        break;
                case 3:
                    if (consecutive == 2)
                        consecutive = 3;
                    result = true;
                    break;
                default:
                    consecutive = 0;
            }
        return result;
    }

    public static String altPairs(String str) {
        String result = "";
        List<Integer> nums = new ArrayList<Integer>();
        int a = 0;
        int b = 1;
        for (int i = 0; i < 10; i++) {
            nums.add(a);
            nums.add(b);
            a += 3;
            b += 3;
        }
        for (char letter : str.toCharArray())
            for (int num : nums)
                if (str.indexOf(letter) == num)
                    result += letter;
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Exercise7Comment.stringTimes("test", 4));
        System.out.println(Exercise7Comment.stringTimes("hi", 1));
        System.out.println(Exercise7Comment.doubleX("xoxo"));
        System.out.println(Exercise7Comment.doubleX("xxxyyyzzz"));
        System.out.println(Exercise7Comment.doubleX("aaaax"));
        System.out.println(Exercise7Comment.last2("hihihihi"));
        System.out.println(Exercise7Comment.last2("aaaaa"));
        System.out.println(Exercise7Comment.array123(new int[]{1, 2, 3, 1, 4}));
        System.out.println(Exercise7Comment.array123(new int[]{1, 4, 3, 2, 3, 1}));
        System.out.println(Exercise7Comment.altPairs("this is some test"));
        System.out.println(Exercise7Comment.altPairs("hi"));
    }
}
