package org.bitbucket.infovillafoundation.wayyan.lesson2.Exercise2.src.main.java;

/**
 * Created by Admin on 2/12/2014.
 */

public class Exercise2 {
    public static boolean startHi(String str) {
        if (str.length() > 1)
            return str.toCharArray()[0] == 'h' && str.toCharArray()[1] == 'i';
        else
            return false;
    }

    public static boolean hasTeen(int a, int b, int c) {
        int[] ages = new int[3];
        ages[0] = a;
        ages[1] = b;
        ages[2] = c;
        for (int i : ages) {
            if (i >= 13 && i <= 19)
                return true;
        }
                return false;
    }

    public static boolean mixStart(String str) {
        if (str.length() > 2)
            return str.toCharArray()[1] == 'i' && str.toCharArray()[2] == 'x';
        else
            return false;
    }

    public static int close10(int a, int b) {
        if (Math.abs(10 - a) > Math.abs(10 - b))
            return b;
        else
            if (Math.abs(10 - a) < Math.abs(10 - b))
                return a;
            else
                return 0;
    }

    public static boolean stringE(String str) {
        int letterNo = 0;
        for (char letter : str.toCharArray()) {
            if (letter == 'e')
                letterNo++;
        }

        if (letterNo >= 1 && letterNo <= 3)
            return true;
        else
            return false;
    }
}
