package org.bitbucket.infovillafoundation.wayyan.lesson3;

/**
 * Created by Admin on 3/12/2014.
 */
public class Exercise3 {
    public static String everyNth(String str, int n) {
        String result = "";
        int originalN = n;
        for (int i = -1; i < str.length() - 1; i++) {
            if (i == n) {
                result += str.toCharArray()[i];
                n += originalN;
            }
        }
        return result;
    }

    public static boolean monkeyTrouble(boolean aSmile, boolean bSmile) {
        return (aSmile && bSmile) || (!aSmile && !bSmile);
    }

    public static boolean parrotTrouble(boolean talking, int hour) {
        return talking && (hour < 7 || hour > 20);
    }

    public static boolean posNeg(int a, int b, boolean negative) {
        return ((a < 0 && b < 0) && negative) || (!negative && ((a < 0 && b >= 0) || (a >= 0 && b < 0)));
    }

    public static String frontBack(String str) {
        String result = "";
        if (str.length() >= 2) {
            if (str.length() == 2) {
                result = Character.toString(str.toCharArray()[1]) + Character.toString(str.toCharArray()[0]);
                return result;
            }
            else
                result = str.toCharArray()[str.length() - 1] + str.substring(1, str.length() - 1) + str.toCharArray()[0];
            return result;
        }
        else
            return str;
    }

    public static void main(String[] args) {
        System.out.println(Exercise3.everyNth("testingstring", 2));
        System.out.println(Exercise3.everyNth("testingstring", 1));
        System.out.println(Exercise3.monkeyTrouble(false, false));
        System.out.println(Exercise3.monkeyTrouble(true, true));
        System.out.println(Exercise3.monkeyTrouble(true, false));
        System.out.println(Exercise3.parrotTrouble(false, 5));
        System.out.println(Exercise3.parrotTrouble(true, 9));
        System.out.println(Exercise3.parrotTrouble(true, 5));
        System.out.println(Exercise3.posNeg(-1, -2, false));
        System.out.println(Exercise3.posNeg(-1, -2, true));
        System.out.println(Exercise3.posNeg(1, -2, false));
        System.out.println(Exercise3.posNeg(1, -2, true));
        System.out.println(Exercise3.frontBack("testing"));
        System.out.println(Exercise3.frontBack("ab"));
        System.out.println(Exercise3.frontBack("a"));
    }
}
