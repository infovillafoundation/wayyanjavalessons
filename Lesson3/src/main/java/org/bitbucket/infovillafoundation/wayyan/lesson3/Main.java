package org.bitbucket.infovillafoundation.wayyan.lesson3;

/**
 * Created by Admin on 4/12/2014.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(Exercise3.everyNth("testingstring", 2));
        System.out.println(Exercise3.everyNth("testingstring", 1));
        System.out.println(Exercise3.monkeyTrouble(false, false));
        System.out.println(Exercise3.monkeyTrouble(true, true));
        System.out.println(Exercise3.monkeyTrouble(true, false));
        System.out.println(Exercise3.parrotTrouble(false, 5));
        System.out.println(Exercise3.parrotTrouble(true, 9));
        System.out.println(Exercise3.parrotTrouble(true, 5));
        System.out.println(Exercise3.posNeg(-1, -2, false));
        System.out.println(Exercise3.posNeg(-1, -2, true));
        System.out.println(Exercise3.posNeg(1, -2, false));
        System.out.println(Exercise3.posNeg(1, -2, true));
        System.out.println(Exercise3.frontBack("testing"));
        System.out.println(Exercise3.frontBack("ab"));
        System.out.println(Exercise3.frontBack("a"));
    }
}
