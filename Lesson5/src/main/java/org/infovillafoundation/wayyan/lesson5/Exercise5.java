package org.infovillafoundation.wayyan.lesson5;

/**
 * Created by Admin on 7/12/2014.
 */
public class Exercise5 {
    public static boolean lastDigit(int a, int b) {
        return (a == b % 10) || (b == a % 10) || (a % 10 == b % 10);
    }

    public static int sumDouble(int a, int b) {
        if (a == b)
            return (a + b) * 2;
        else
            return a + b;
    }

    public static boolean makes10(int a, int b) {
        return ((a == 10 && b != 10) || (a != 10 && b == 10)) || (a == 10 && b == 10) || a + b == 10;
    }

    public static String notString(String str) {
        if (str.length() > 2)
            if (str.substring(0, 3).equals("not"))
                return str;
            else
                return "not " + str;
        else
            return "not " + str;
    }

    public static String front3(String str) {
        String result = "";
        if (str.length() >= 3)
            for (int i = 0; i < 3; i++)
                result += str.substring(0, 3);
        else
            for (int i = 0; i < 3; i++)
                result += str;
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Exercise5.lastDigit(13, 103));
        System.out.println(Exercise5.lastDigit(13, 104));
        System.out.println(Exercise5.lastDigit(14, 103));
        System.out.println(Exercise5.sumDouble(10, 10));
        System.out.println(Exercise5.sumDouble(10, 11));
        System.out.println(Exercise5.makes10(1, 9));
        System.out.println(Exercise5.makes10(9, 10));
        System.out.println(Exercise5.makes10(9, 9));
        System.out.println(Exercise5.makes10(10, 10));
        System.out.println(Exercise5.notString("testing at all"));
        System.out.println(Exercise5.notString("not really hard"));
        System.out.println(Exercise5.notString("this will not work"));
        System.out.println(Exercise5.notString("c"));
        System.out.println(Exercise5.front3("yay"));
        System.out.println(Exercise5.front3("testing"));
        System.out.println(Exercise5.front3("a"));
    }
}
