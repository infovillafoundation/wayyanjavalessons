package org.bitbucket.infovillafoundation.wayyan.projecttests;

/**
 * Created by Admin on 28/12/2014.
 */

public class Main {
    public static void main(String[] args) {
        ClassField test1 = new ClassField();
        ClassField test2 = new ClassField();

        System.out.println(test1.getValue());
        test1.setValue(20);
        System.out.println(test1.getValue());

        System.out.println(test2.a);
        test2.a = 20;
        System.out.println(test2.a);
    }
}
