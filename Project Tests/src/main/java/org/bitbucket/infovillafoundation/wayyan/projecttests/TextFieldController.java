package org.bitbucket.infovillafoundation.wayyan.projecttests;

import javafx.fxml.FXML;
import org.datafx.controller.FXMLController;

import java.awt.*;

/**
 * Created by Admin on 20/12/2014.
 */

@FXMLController(value="/fxml/visibility.fxml", title="Text Field Test")
public class TextFieldController {
    @FXML
    private Label enter;

    @FXML
    private TextField text;

    @FXML
    private Button finish;

    public static void main(String[] args) {
        System.out.println();
    }
}
