package org.bitbucket.infovillafoundation.wayyan.projecttests;

/**
 * Created by Admin on 28/12/2014.
 */

public class ClassField {
    int a = 10;

    public int getValue() {
        return a;
    }

    public void setValue(int a) {
        this.a = a;
    }
}
