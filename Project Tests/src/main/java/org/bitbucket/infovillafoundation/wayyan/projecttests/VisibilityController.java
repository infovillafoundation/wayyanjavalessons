package org.bitbucket.infovillafoundation.wayyan.projecttests;

import javafx.fxml.FXML;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;

import javax.annotation.PostConstruct;

/**
 * Created by Admin on 17/12/2014.
 */

@FXMLController(value="/fxml/visibility.fxml", title="Visibility Test")
public class VisibilityController {
    int control = 1;

    @FXML
    private Text invisible;

    @FXML
    private Text visible;

    @PostConstruct
    public void init() {
        if (control == 1) {
            invisible.setVisible(false);
            visible.setVisible(true);
        }
    }
}
