import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 14/12/2014.
 */

public class ListToString {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<String>();

        strings.add("testing");
        strings.add("some");
        strings.add("stuff");

        System.out.println(strings.toString()); // Prints the elements out surrounded by square brackets
    }
}
