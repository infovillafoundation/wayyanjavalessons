import java.lang.String;

/**
 * Created by Admin on 2/12/2014.
 */

public class Patterns {
    public static String makeStarTriangle(int rows) {
        String result = "";
        String stars = "";
        for (int i = 0; i < rows + 1; i++) {
             stars = "";
             for (int j = 0; j < i; j++)
                 stars += "*";
            result += stars + "\n";
        }
        return result;
    }

    public static String makeNumberTriangle(int rows) {
        int numbersInRow = 0;
        String result = "";
        String row = "";
        int outputNo = 1;
        for (int i = 0; i < ((rows * (rows + 1)) / 2) + 1; i++) {
            if (outputNo > 9)
                outputNo = 1;
            if (row.length() > numbersInRow) {
                result += row + "\n";
                numbersInRow++;
                row = "";
            }
            row += outputNo;
            outputNo++;
        }
        return result;
    }

    public static String makeNumberTriangle2(int rows) {
        int numbersInRow = 0;
        String result = "";
        String row = "";
        int outputNo = 1;
        boolean increment = true;
        for (int i = 0; i < ((rows * (rows + 1)) / 2) + 1; i++) {
            row += outputNo;
            if (increment) {
                outputNo++;
                if (outputNo == 9)
                    increment = false;
            }
            else {
                outputNo--;
                if (outputNo == 1)
                    increment = true;
            }

            if (row.length() > numbersInRow) {
                result += row + "\n";
                numbersInRow++;
                row = "";
            }
        }
        return result;
    }

    public static String makeNumberTriangle3(int rows) {
        int numbersInRow = 0;
        String result = "";
        String row = "";
        int outputNo = 1;
        boolean increment = true;
        String spaces = "";
        for (int i = 0; i < ((rows * (rows + 1)) / 2) + 1; i++) {
            spaces = "";

            for (int j = 0; j < rows - i; j++) {
                spaces += " ";
            }

            row += spaces + outputNo;
            if (increment) {
                outputNo++;
                if (outputNo == 9)
                    increment = false;
            }
            else {
                outputNo--;
                if (outputNo == 1)
                    increment = true;
            }

            if (row.length() > numbersInRow) {
                result += row + "\n";
                numbersInRow++;
                row = "";
            }
        }
        return result;
    }

    /*public static String makeChessBoard(int rows) {
        int rowNo = 1;
        String row = "# # # #";
        String result = "";
        for (int i = 0; i < rows; i++) {
            if (rowNo == 1)
                result += row;
            else
                result += " " + row;
            rowNo++;
        }
        return result;
    }*/

    public static void main(String[] args) {
        System.out.println(makeStarTriangle(10));
        System.out.println(makeNumberTriangle(10));
        System.out.println(makeNumberTriangle2(10));
        System.out.println(makeNumberTriangle3(10));
        /*System.out.println(makeChessBoard(10));*/
    }
}
