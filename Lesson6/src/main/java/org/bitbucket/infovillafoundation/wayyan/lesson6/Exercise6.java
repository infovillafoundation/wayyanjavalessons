package org.bitbucket.infovillafoundation.wayyan.lesson6;

/**
 * Created by Admin on 8/12/2014.
 */

public class Exercise6 {
    public static boolean in1020(int a, int b) {
        return (a >= 10 && a <= 20) || (b >= 10 && b <= 20);
    }

    public static String delDel(String str) {
        if (str.length() < 4)
            return str;
        if (str.indexOf("del") == 1) {
            return str.replace("del", "");
        }
        return str;
    }

    public static int intMax(int a, int b, int c) {
        return Math.max(Math.max(a, b), c);
    }

    public static int max1020(int a, int b) {
        if (Math.max(a, b) >= 10 && Math.max(a, b) <= 20)
            return Math.max(a, b);
        if (!(Math.min(a, b) >= 10 && Math.min(a, b) <= 20) && !(Math.max(a, b) >= 10 && Math.max(a, b) <= 20))
            return Math.min(a, b);
        return 0;
    }

    public static String endUp(String str) {
        if (str.length() > 2) {
            String last3 = str.substring(str.length() - 3);
            System.out.println(last3);
            return str.replace(last3, last3.toUpperCase());
        }
        else
            return str.toUpperCase();
    }

    public static String front22(String str) {
        if (str.length() > 1)
            return str.substring(0, 2) + str + str.substring(0, 2);
        else
            return str + str + str;
    }

    public static void main(String[] args) {
        System.out.println(in1020(15, 35));
        System.out.println(in1020(15, 16));
        System.out.println(in1020(5, 35));
        System.out.println(in1020(20, 20));
        System.out.println(delDel("don't delete this"));
        System.out.println(delDel("this string will remain"));
        System.out.println(delDel("dell computers remain"));
        System.out.println(intMax(1, 2, 3));
        System.out.println(intMax(2, 1, 3));
        System.out.println(max1020(5, 55));
        System.out.println(max1020(5, 15));
        System.out.println(max1020(15, 25));
        System.out.println(max1020(15, 16));
        System.out.println(endUp("uppercase him"));
        System.out.println(endUp("hi"));
        System.out.println(front22("the"));
        System.out.println(front22("abababab"));
        System.out.println(front22("a"));
    }
}
