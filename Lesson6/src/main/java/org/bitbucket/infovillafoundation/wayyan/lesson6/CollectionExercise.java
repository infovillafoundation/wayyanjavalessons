 package org.bitbucket.infovillafoundation.wayyan.lesson6;

import java.util.*;

/**
 * Created by Admin on 8/12/2014.
 */
public class CollectionExercise {
    public static void main(String[] args) {
        // Exercise No.1
        Set<Integer> set = new HashSet<Integer>();
        set.add(0);
        set.add(1);
        set.add(2);
        set.add(3);

        for (Integer num : set)
            System.out.println(num);

        // Exercise No.3
        List<String> strings = new ArrayList<String>();
        strings.add(" hi ");
        strings.add("          bye            ");
        strings.add("1 2 3 4");
        for (String strElement : strings) {
            System.out.println(strElement.trim());
        }
    }
}
