package org.bitbucket.infovillafoundation.wayyan.javafxtest;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.datafx.controller.flow.Flow;
import org.datafx.controller.flow.action.FlowActionChain;
import org.datafx.controller.flow.action.FlowLink;
import org.datafx.controller.flow.action.FlowMethodAction;

/**
 * Created by Admin on 9/12/2014.
 */
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.getIcons().add(new Image("/images/icon.png"));
        new Flow(WelcomeController.class)
                .withAction(Question1Controller.class, "chooseAndNext", new FlowActionChain(new FlowMethodAction("makeChoice"), new FlowLink<>(Question2Controller.class)))
                .withAction(Question2Controller.class, "chooseAndNext", new FlowActionChain(new FlowMethodAction("makeChoice"), new FlowLink<>(Question3Controller.class)))
                .withAction(Question3Controller.class, "chooseAndNext", new FlowActionChain(new FlowMethodAction("makeChoice"), new FlowLink<>(Question4Controller.class)))
                .withAction(Question4Controller.class, "chooseAndNext", new FlowActionChain(new FlowMethodAction("makeChoice"), new FlowLink<>(Question5Controller.class)))
                .withAction(Question5Controller.class, "chooseAndNext", new FlowActionChain(new FlowMethodAction("makeChoice"), new FlowLink<>(Question6Controller.class)))
                .withAction(Question6Controller.class, "chooseAndNext", new FlowActionChain(new FlowMethodAction("makeChoice"), new FlowLink<>(Question7Controller.class)))
                .withAction(Question7Controller.class, "chooseAndNext", new FlowActionChain(new FlowMethodAction("makeChoice"), new FlowLink<>(Question8Controller.class)))
                .withAction(Question8Controller.class, "chooseAndNext", new FlowActionChain(new FlowMethodAction("makeChoice"), new FlowLink<>(Question9Controller.class)))
                .withAction(Question9Controller.class, "chooseAndNext", new FlowActionChain(new FlowMethodAction("makeChoice"), new FlowLink<>(Question10Controller.class)))
                .withAction(Question10Controller.class, "chooseAndNext", new FlowActionChain(new FlowMethodAction("makeChoice"), new FlowLink<>(FinishController.class)))
                .startInStage(primaryStage);
    }
}
