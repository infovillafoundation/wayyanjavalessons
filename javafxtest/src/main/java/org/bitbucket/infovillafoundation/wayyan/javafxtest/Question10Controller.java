package org.bitbucket.infovillafoundation.wayyan.javafxtest;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;
import org.datafx.controller.flow.action.BackAction;
import org.datafx.controller.flow.action.LinkAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by Admin on 9/12/2014.
 */

@FXMLController(value="/fxml/question10.fxml", title="Question 10")
public class Question10Controller {
    @Inject
    private Result result;

    @FXML
    private Text question10;

    @FXML
    private ToggleGroup choices10;

    @FXML
    private RadioButton option10a;

    @FXML
    private RadioButton option10b;

    @FXML
    @BackAction
    private Button back;

    @FXML
    @ActionTrigger("chooseAndNext")
    private Button next;

    @PostConstruct
    public void init() {
        choices10.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                next.setVisible(true);
        }
        });
    }

    @ActionMethod("makeChoice")
    public void makeChoice() {
        if (choices10.getSelectedToggle() == option10a)
            result.addChoice(10, "a");
        else
            result.addChoice(10, "b");
    }
}