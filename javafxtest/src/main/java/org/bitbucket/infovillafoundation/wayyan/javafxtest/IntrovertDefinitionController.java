package org.bitbucket.infovillafoundation.wayyan.javafxtest;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.BackAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by Admin on 9/12/2014.
 */

@FXMLController(value="/fxml/definition.fxml", title="Definition")
public class IntrovertDefinitionController {
    @Inject
    private Result result;

    @FXML
    private Text definition;

    @FXML
    @BackAction
    private Button back;

    @PostConstruct
    public void init() {
        definition.setText("Introvert Personality: Individuals who prefer to remain isolated or in the company of very few people, can be categorized as ones who have an introverted personality.");
    }
}
