package org.bitbucket.infovillafoundation.wayyan.javafxtest;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;
import org.datafx.controller.flow.action.BackAction;
import org.datafx.controller.flow.action.LinkAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by Admin on 9/12/2014.
 */

@FXMLController(value="/fxml/question7.fxml", title="Question 7")
public class Question7Controller {
    @Inject
    private Result result;

    @FXML
    private Text question7;

    @FXML
    private ToggleGroup choices7;

    @FXML
    private RadioButton option7a;

    @FXML
    private RadioButton option7b;

    @FXML
    @BackAction
    private Button back;

    @FXML
    @ActionTrigger("chooseAndNext")
    private Button next;

    @PostConstruct
    public void init() {
        choices7.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                next.setVisible(true);
            }
        });
    }

    @ActionMethod("makeChoice")
    public void makeChoice() {
        if (choices7.getSelectedToggle() == option7a)
            result.addChoice(7, "a");
        else
            result.addChoice(7, "b");
    }
}