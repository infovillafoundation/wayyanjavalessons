package org.bitbucket.infovillafoundation.wayyan.javafxtest;

import org.datafx.controller.injection.ApplicationScoped;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Admin on 9/12/2014.
 */

@ApplicationScoped
public class Result {
    List<String> choices = new ArrayList<>();

    public void addChoice(int questionNumber, String choice) {
        choices.add(questionNumber - 1, choice);
    }

    public String countChoices() {
        int a = 0;
        int b = 0;
        for (String choice : choices) {
            if (choice.equalsIgnoreCase("a"))
                a++;
            else
                b++;
        }
        if (a > b)
            return "a";
        else
            return "b";
    }
}
