package org.bitbucket.infovillafoundation.wayyan.javafxtest;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.LinkAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by Admin on 9/12/2014.
 */

@FXMLController(value="/fxml/finish.fxml", title="Thank You!")
public class FinishController {
    @Inject
    private Result result;

    @FXML
    private Text finish;

    @FXML
    private Text recorded;

    @FXML
    private Text personality;

    @FXML
    @LinkAction(ExtrovertDefinitionController.class)
    private Button extrovert;

    @FXML
    @LinkAction(IntrovertDefinitionController.class)
    private Button introvert;

    @FXML
    @LinkAction(WelcomeController.class)
    private Button back;

    @PostConstruct
    public void init() {
        String personalityResult = result.countChoices();
        if (personalityResult.equals("a"))
            personality.setText("extrovert.");
        else
            personality.setText("introvert.");
    }
}

