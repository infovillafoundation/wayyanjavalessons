package org.bitbucket.infovillafoundation.wayyan.javafxtest;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;
import org.datafx.controller.flow.action.BackAction;
import org.datafx.controller.flow.action.LinkAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by Admin on 9/12/2014.
 */

@FXMLController(value="/fxml/question8.fxml", title="Question 8")
public class Question8Controller {
    @Inject
    private Result result;

    @FXML
    private Text question8;

    @FXML
    private ToggleGroup choices8;

    @FXML
    private RadioButton option8a;

    @FXML
    private RadioButton option8b;

    @FXML
    @BackAction
    private Button back;

    @FXML
    @ActionTrigger("chooseAndNext")
    private Button next;

    @PostConstruct
    public void init() {
        choices8.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                next.setVisible(true);
            }
        });
    }

    @ActionMethod("makeChoice")
    public void makeChoice() {
        if (choices8.getSelectedToggle() == option8a)
            result.addChoice(8, "a");
        else
            result.addChoice(8, "b");
    }
}