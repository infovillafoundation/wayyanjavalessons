package org.bitbucket.infovillafoundation.wayyan.javafxtest;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;
import org.datafx.controller.flow.action.BackAction;
import org.datafx.controller.flow.action.LinkAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by Admin on 9/12/2014.
 */

@FXMLController(value="/fxml/question5.fxml", title="Question 5")
public class Question5Controller {
    @Inject
    private Result result;

    @FXML
    private Text question5;

    @FXML
    private ToggleGroup choices5;

    @FXML
    private RadioButton option5a;

    @FXML
    private RadioButton option5b;

    @FXML
    @BackAction
    private Button back;
    @FXML

    @ActionTrigger("chooseAndNext")
    private Button next;

    @PostConstruct
    public void init() {
        choices5.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                next.setVisible(true);
            }
        });
    }

    @ActionMethod("makeChoice")
    public void makeChoice() {
        if (choices5.getSelectedToggle() == option5a)
            result.addChoice(5, "a");
        else
            result.addChoice(5, "b");
    }
}