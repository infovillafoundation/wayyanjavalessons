package org.bitbucket.infovillafoundation.wayyan.personalitytest;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 * Created by Admin on 14/12/2014.
 */

@NamedQueries({
    @NamedQuery(name="Questions.findQuestions", query="SELECT question FROM personality_test"),
    @NamedQuery(name="Questions.findChoice1", query="SELECT choice1 FROM personality_test"),
    @NamedQuery(name="Questions.findChoice2", query="SELECT choice2 FROM personality_test")
})
@Entity
public class Questions {
    private int totalNo = 50;
    private int questionNumber = 0;
    private List<Questions> questions = new ArrayList<>();
    private List<String> choices = new ArrayList<>();

    public void addChoice(int questionNumber, String choice) {
        choices.add(questionNumber - 1, choice);
    }

    public void addQuestion(int questionNumber, Questions question) {
        questions.add(questionNumber - 1, question);
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public int getTotalNo() {
        return totalNo;
    }

    public void setQuestionNumber(int questionNumber) {
        this.questionNumber = questionNumber;
    }
}
