package org.bitbucket.infovillafoundation.wayyan.personalitytest;

import com.sun.org.apache.xpath.internal.operations.*;

import javax.persistence.*;
import java.lang.String;
import java.util.List;

/**
 * Created by Admin on 20/12/2014.
 */
public class QuestionDao extends Dao {
    public static List<String> getQuestions() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<String> query = em.createNamedQuery("Questions.findQuestion", Questions.class);
        List<String> questions = query.getResultList();
        return questions;
    }

    public static List<String> getChoice1() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<String> query = em.createNamedQuery("Questions.findChoice1", Questions.class);
        List<String> choice1 = query.getResultList();
        return choice1;
    }

    public static List<String> getChoice2() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<String> query = em.createNamedQuery("Questions.findChoice2", Questions.class);
        List<String> choice2 = query.getResultList();
        return choice2;
    }
}
