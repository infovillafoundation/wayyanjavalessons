package org.bitbucket.infovillafoundation.wayyan.personalitytest;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.LinkAction;

import javax.annotation.PostConstruct;

/**
 * Created by Admin on 28/12/2014.
 */

@FXMLController(value="/fxml/definition.fxml", title="Definitions")
public class DefinitionController {
    @FXML
    private FinishController finishController;
    
    @FXML
    private Text extra;

    @FXML
    private Text intro;

    @FXML
    private Text sens;

    @FXML
    private Text intu;

    @FXML
    private Text thin;

    @FXML
    private Text feel;

    @FXML
    private Text judg;

    @FXML
    private Text perc;

    @FXML
    private Text extraDef;

    @FXML
    private Text introDef;

    @FXML
    private Text sensDef;

    @FXML
    private Text intuDef;

    @FXML
    private Text thinDef;

    @FXML
    private Text feelDef;

    @FXML
    private Text judgDef;

    @FXML
    private Text percDef;
    
    @FXML
    @LinkAction(WelcomeController.class)
    private Button menu;
    
    @PostConstruct
    public void init() {
        if (finishController.questionController.result.calculateResult().toArray()[0] == "extraverted") {
            extra.setVisible(true);
            intro.setVisible(false);
            extraDef.setVisible(true);
            introDef.setVisible(false);
        }
        else {
            extra.setVisible(false);
            intro.setVisible(true);
            extraDef.setVisible(false);
            introDef.setVisible(true);
        }

        if (finishController.questionController.result.calculateResult().toArray()[1] == "sensitive") {
            sens.setVisible(true);
            intu.setVisible(false);
            sensDef.setVisible(true);
            intuDef.setVisible(false);
        }
        else {
            sens.setVisible(false);
            intu.setVisible(true);
            sensDef.setVisible(false);
            intuDef.setVisible(true);
        }

        if (finishController.questionController.result.calculateResult().toArray()[2] == "thinking") {
            thin.setVisible(true);
            feel.setVisible(false);
            thinDef.setVisible(true);
            feelDef.setVisible(false);
        }
        else {
            thin.setVisible(false);
            feel.setVisible(true);
            thinDef.setVisible(false);
            feelDef.setVisible(true);
        }

        if (finishController.questionController.result.calculateResult().toArray()[3] == "judging") {
            judg.setVisible(true);
            perc.setVisible(false);
            judgDef.setVisible(true);
            percDef.setVisible(false);
        }
        else {
            judg.setVisible(false);
            perc.setVisible(true);
            judgDef.setVisible(false);
            percDef.setVisible(true);
        }
    }
}
