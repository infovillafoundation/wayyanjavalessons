package org.bitbucket.infovillafoundation.wayyan.personalitytest;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 14/12/2014.
 */

public class Result {
    private List<String> results = new ArrayList<>(70);
    private List<String> personality = new ArrayList<>(4);
    private Integer[] col1 = {1, 8, 15, 22, 29, 36, 43, 50, 57, 64};
    private Integer[] col2 = {2, 3, 9, 10, 16, 17, 23, 24, 30, 31, 37, 38, 44, 45, 51, 52, 58, 59, 65, 66};
    private Integer[] col3 = {4, 5, 11, 12, 18, 19, 25, 26, 32, 33, 39, 40, 46, 47, 53, 54, 60, 61, 67, 68};
    private Integer[] col4 = {6, 7, 13, 14, 20, 21, 27, 28, 34, 35, 41, 42, 48, 49, 55, 56, 62, 63, 69, 70};

    public void addResult(int indexNo, String choice) {
        results.toArray()[indexNo - 1] = choice;
    }

    public List<String> calculateResult() {
        int aNo = 0;
        int bNo = 0;
        int cNo = 0;
        int dNo = 0;
        int eNo = 0;
        int fNo = 0;
        int gNo = 0;
        int hNo = 0;
        for (int i : col1) {
            if (results.toArray()[i - 1] == "a") {
                aNo++;
            }
            else {
                bNo++;
            }
        }
        for (int i : col2) {
            if (results.toArray()[i - 1] == "a") {
                cNo++;
            }
            else {
                dNo++;
            }
        }
        for (int i : col3) {
            if (results.toArray()[i - 1] == "a") {
                eNo++;
            }
            else {
                fNo++;
            }
        }
        for (int i : col4) {
            if (results.toArray()[i - 1] == "a") {
                gNo++;
            }
            else {
                hNo++;
            }
        }

        if (aNo > bNo) {
            personality.toArray()[0] = "extraverted";
        }
        else {
            personality.toArray()[0] = "introverted";
        }

        if (cNo > dNo) {
            personality.toArray()[1] = "sensitive";
        }
        else {
            personality.toArray()[1] = "intuitive";
        }

        if (eNo > fNo) {
            personality.toArray()[2] = "thinking";
        }
        else {
            personality.toArray()[2] = "feeling";
        }

        if (gNo > hNo) {
            personality.toArray()[3] = "judging";
        }
        else {
            personality.toArray()[3] = "perceiving";
        }

        return personality;
    }
}
