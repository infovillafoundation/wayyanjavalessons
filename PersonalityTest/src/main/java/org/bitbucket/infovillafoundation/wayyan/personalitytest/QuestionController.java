package org.bitbucket.infovillafoundation.wayyan.personalitytest;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;
import org.datafx.controller.flow.action.BackAction;
import org.datafx.controller.flow.action.LinkAction;
import org.datafx.controller.injection.ApplicationScoped;


import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.lang.annotation.Repeatable;

/**
 * Created by Admin on 14/12/2014.
 */

@ApplicationScoped
@FXMLController(value="/fxml/question.fxml", title="Questions")
public class QuestionController {
    @Inject
    private Questions questions;

    @Inject
    public Result result;

    @FXML
    private Text question;

    @FXML
    private ToggleGroup choices;

    @FXML
    private RadioButton optiona;

    @FXML
    private RadioButton optionb;

    @FXML
    @BackAction
    private Button menu;

    @FXML
    @BackAction
    private Button back;

    @FXML
    @ActionTrigger("chooseAndNext")
    private Button next;

    @FXML
    @LinkAction(FinishController.class)
    private Button finish;

    @ActionMethod("chooseAndNext")
    public void chooseAndNext() {
        questions.setQuestionNumber(questions.getQuestionNumber() + 1);
    }

    @PostConstruct
    public void init() {
        if (questions.getQuestionNumber() == 1) {
            menu.setVisible(true);
            back.setVisible(false);
            next.setVisible(false);
            finish.setVisible(false);
        }
        else if (questions.getQuestionNumber() > 1 && questions.getQuestionNumber() < 70) {
            menu.setVisible(false);
            back.setVisible(true);
            next.setVisible(false);
            finish.setVisible(false);
        }
        else if (questions.getQuestionNumber() == 70) {
            next.setVisible(false);
            finish.setVisible(false);
        }

       choices.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (questions.getQuestionNumber() < 70) {
                    next.setVisible(true);
                    finish.setVisible(false);
                }
                else {
                    next.setVisible(false);
                    finish.setVisible(true);
                }
            }
        });
    }

    @ActionMethod("makeChoice")
    public void makeChoice() {
        if (choices.getSelectedToggle() == optiona)
            result.addResult(questions.getQuestionNumber(), "a");
        else
            result.addResult(questions.getQuestionNumber(), "b");
    }
}