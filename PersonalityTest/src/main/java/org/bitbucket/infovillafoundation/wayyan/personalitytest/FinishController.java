package org.bitbucket.infovillafoundation.wayyan.personalitytest;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.LinkAction;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by Admin on 14/12/2014.
 */

@FXMLController(value="/fxml/finish.fxml", title="Congratulations!")
public class FinishController {
    @Inject
    public QuestionController questionController;

    @FXML
    private Text result;

    @FXML
    @LinkAction(DefinitionController.class)
    private Button definition;

    @PostConstruct
    public void init() {
        if (questionController.result.calculateResult().toArray()[0] == "extraverted") {
            result.setText(result + "extraverted, ");
        }
        else {
            result.setText(result + "introverted, ");
        }

        if (questionController.result.calculateResult().toArray()[1] == "sensitive") {
            result.setText(result + "sensitive, ");
        }
        else {
            result.setText(result + "intuitive, ");
        }

        if (questionController.result.calculateResult().toArray()[2] == "thinking") {
            result.setText(result + "thinking, and ");
        }
        else {
            result.setText(result + "feeling, and");
        }

        if (questionController.result.calculateResult().toArray()[3] == "judging") {
            result.setText(result + "judging.");
        }
        else {
            result.setText(result + "perceiving");
        }
    }
}
