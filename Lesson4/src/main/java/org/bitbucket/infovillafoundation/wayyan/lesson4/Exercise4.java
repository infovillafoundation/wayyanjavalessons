package org.bitbucket.infovillafoundation.wayyan.lesson4;

/**
 * Created by Admin on 4/12/2014.
 */
public class Exercise4 {
    public static boolean or35(int n) {
        return Math.abs(n) % 3 == 0 || Math.abs(n) % 5 == 0;
    }

    public static boolean icyHot(int temp1, int temp2) {
        return (temp1 < 0 && temp2 > 100) || (temp1 > 100 && temp2 < 0);
    }

    public static boolean loneTeen(int a, int b) {
        return (a >= 13 && a <= 19 && (b < 13 || b > 19)) || ((a < 13 || a > 19) && b >= 13 && b <= 19);
    }

    public static String startOz(String str) {
        if (str.length() >= 2) {
            if (str.toCharArray()[0] == 'o' && str.toCharArray()[1] == 'z')
                return "oz";
            if (str.toCharArray()[0] == 'o')
                return "o";
            if (str.toCharArray()[1] == 'z')
                return "z";
            else
                return "";
            }
        if (str.length() == 1)
            if (str.toCharArray()[0] == 'o')
                return "o";
            else
                return "";
        else
            return "";
    }

    public static boolean in3050(int a, int b) {
        return (((a >= 30 && a <= 40) && (b >= 30 && b <= 40)) || ((a >= 40 && a <= 50) && (b >= 40 && b <= 50)));
    }

    public static void main(String[] args) {
        System.out.println(Exercise4.or35(99));
        System.out.println(Exercise4.or35(100));
        System.out.println(Exercise4.or35(15));
        System.out.println(Exercise4.or35(4));
        System.out.println(Exercise4.icyHot(-1, 101));
        System.out.println(Exercise4.icyHot(-1, -1));
        System.out.println(Exercise4.icyHot(101, 101));
        System.out.println(Exercise4.icyHot(101, -1));
        System.out.println(Exercise4.loneTeen(13, 20));
        System.out.println(Exercise4.loneTeen(13, 13));
        System.out.println(Exercise4.loneTeen(19, 19));
        System.out.println(Exercise4.loneTeen(20, 13));
        System.out.println(Exercise4.startOz("o"));
        System.out.println(Exercise4.startOz("oz"));
        System.out.println(Exercise4.startOz("az"));
        System.out.println(Exercise4.startOz("oa"));
        System.out.println(Exercise4.in3050(30, 40));
        System.out.println(Exercise4.in3050(30, 50));
        System.out.println(Exercise4.in3050(40, 50));
    }
}
